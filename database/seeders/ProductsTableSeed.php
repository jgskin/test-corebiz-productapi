<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 2; $i++) {
            Product::create([
                'name' => $faker->sentence(2),
                'description' => $faker->paragraph,
                'price' => $faker->randomNumber(4),
            ]);
        }
    }
}
