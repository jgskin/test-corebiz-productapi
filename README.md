# Test Corebiz API de produtos

Passos para iniciar os containers:

1. Criar o arquivo docker/.env aos moldes do docker/.env.sample
2. Criar arquivo docker/appenv aos moldes do docker/appenv.sample
3. Executar "docker-compose up"
4. Executar "docker-compose exec app php artisan migrate"
5. Executar "docker-compose exec app php artisan db:seed"
6. Pronto, a api deve estar rodando em localhost:8000/api/products